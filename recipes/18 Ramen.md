# Ramen

A mouthwatering noodle soup, filled with vibrant vegetables and runny boiled eggs.
Serves 2.

## Ingredients
- 1 handful fresh coriander
- 1/4 broccoli (chopped into medium chunks)
- 1/2 tsp chili powder
- 1 tbsp sesame seeds
- 2 tbsp soy sauce
- 3 Eggs (6 mins)
- Vegetable stock
- 1 tbsp fresh ginger (grated)
- 2 cloves garlic (crushed)
- 1/2 leek
- 120g shiitake mushrooms (stems removed, torn in half)
- 2 nests of noodles
- Oil
- 6 spring onions (finely sliced)

## Steps
1. Fry the garlic, 4 spring onions, ginger, mushrooms and soy sauce in a pan on high heat for a few minutes.
2. Add 500ml of vegetable stock, coriander, leek (no need to chop), a generous seasoning of salt, and chili powder.
3. Bring to a boil, then cover and simmer for at least 30 minutes to allow the flavours to combine, if necessary more water can be added.
4. Add the noodles to the broth.
4. While the noodles cook, boil 3 eggs for 6 minutes (so the yolk is runny), then let rest in a pan of cold water.
5. Using a ladle, transfer the broth to two bowls, leaving room for the eggs.
6. Peel the eggs and slice in half, placing 3 halves in each bowl.
7. Season the broth with sesame seeds and the remaining spring onions.