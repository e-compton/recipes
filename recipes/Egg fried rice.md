# Egg Fried Rice

Delicious fried rice, make some leftover rice more exciting.

## Ingredients
- 2 cups of cooked white rice
- 1 tbsp of soy sauce
- 3 spring onions (diced)
- 1 whole egg (cracked in a cup ready)

## Steps
1. Get a wok or frying pan on a high heat and wait until its very hot.
2. Add the oil, rice, spring onions, and soy sauce stirring vigorously.
3. Add the egg, breaking apart the yoke, and continuing to stir vigorously so that as much rice as possible is covered as the egg cooks.
4. Continue to cook, mixing frequently until the egg is completely cooked and the rice looks dry (about 5min).