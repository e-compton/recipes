# Rosemary and Red onion Focaccia

Makes a large loaf of focaccia that works perfectly with houmous or as a side to a meal.
(requires proving overnight)
Serves 4.

## Ingredients
- 600ml water
- 850g bread flour
- 1/2 red onion
- olive oil
- 1 handful fresh rosemary
- 6g yeast
- sugar

## Method
1. Mix the dough and water together in a bowl.
2. Whisk the yeast with some warm water and a pinch of sugar, let stand for 5 minutes then add to the dough.
3. Add 1 and 1/2 tbsp of salt and knead for about 7 minutes. The dough will be very sticky.
4. Generously oil a large bowl, put the dough in the bowl to rise overnight.
5. Pour the dough onto a generously oiled oven tray.
6. Fold the corners of the dough over itself and let rest until the dough will easily stretch to fit the tray.
7. Mix together about 100ml of warm water and a tbsp salt, pour over the focaccia dough.
8. Using your hands, poke the dough to give it a bumpy texture.
9. Sprinkle over the rosemary and finely sliced red onion.
10. Cook at 210 degrees celsius for half an hour, sprinkling a little more salt on halfway, let cool on a rack.