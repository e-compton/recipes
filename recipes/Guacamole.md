# Guacamole

Classic, chunky guacamole.

## Ingredients
- 1 avocado
- 1/2 lime

Optional:
- 1/4 red onion (finely diced)
- A small handful of tomatoes (finely diced)

## Steps
1. Remove the avocado stone, scoop out the inside and mash in a bowl or blend.
2. Add juice of lime, tomatoes and red onion.
3. Mix and add salt to taste.
