# Granola

Makes a large jar of crunchy granola, perfect served with greek yoghurt, on its own or with milk.
This is a flexible recipe where you can change the ingredients as desired!

## Check quantities
## Ingredients
- 200ml honey
- 1 vanilla pod
- 300g oats
- 100g rice krispies
- 75g pumpkin seeds
- 75g sunflower seeds
- 50g flax seeds
- 50g Goji berries
- 50g cranberries

## Method
1. Scrape the seeds out of the vanilla pod, put in a small saucepan with the honey.
1. Warm up the honey and vanilla for a couple of minutes on a low heat.
2. Mix the nuts, seeds, rice krispies and oats in a bowl.
3. Stir in the honey.
3. Spread the mix thinly on a large baking tray, season with salt.
4. Put into the oven for 30 minutes at 180 degrees, stirring the mixture halfway.
5. Stir in the fruit and store in a sealed container once cool.