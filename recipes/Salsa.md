# Salsa

The perfect addition to a Mexican feast.

## Ingredients
- 1/2 red onion
- A handful of tomatoes (finely chopped)
- A handful of fresh coriander (roughly chopped)
- White wine vinegar

## Steps
1. Mix together the red onion, tomatoes, coriander and half a teaspoon of white wine vinegar.
2. Season with salt to taste.