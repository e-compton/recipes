# Refried beans

A tasty Mexican treat!

## Ingredients
- 1 can kidney beans
- 1 tsp butter
- 2 cloves garlic (crushed)
- 1 onion (finely diced)
- 1/2 tsp chili powder

## Steps
1. Fry the garlic and onion in the butter until soft.
2. Add the beans, about 200ml of water and a generous sprinkle of salt.
3. Let simmer for at least twenty minutes, adding water when necessary. While cooking,to attain a smoother consistency, you can mash/blend some or all of the beans.
