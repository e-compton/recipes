# Salmon and Taboulleh

## Ingredients
- 2 Salmon Steaks (as fresh as possible)
- 1 Lemon (as fresh as possible, lighter yellow means fresher)
- 200g Baby Tomatoes (very finely diced)
- Half a Cucumber (very finely diced)
- 2 Spring Onions (finely sliced)
- 150g Couscous
- 100g Fresh Flat Leaf Parsley
- 1 Vegetable Stock Cube
- Olive Oil
- Two cloves of garlic

## Method
1. Take the Salmon steaks out the Fridge and pat dry with kitchen towel and coat all sides with salt.
2. Mix the Tomatoes, Cucumber, Onions and Parsley together in a large bowl.
3. Add the stock cube to 200ml of boiling water, whisk with a fork until dissolved.
4. In a saucepan on a low heat, add the couscous and water. Cook for 4min or until dry.
5. Let the couscous cool for a few minutes before adding it to the rest of the ingredients.
6. Grate over the entire zest of the lemon.
7. Mix thoroughly, adding a splash of olive oil, salt, and squeezing in half of the lemon.
8. Leave Taboulleh to rest while preparing the Salmon.
9. Put a non-stick pan on the hob at medium heat.
10. Once the pan is hot, add the oil and place the two Salmon steaks skin side down away from you into the pan. If they don't sizzle as you place them then the pan is not hot enough yet.
11. Lightly press the garlic cloves with the back of a knife and place them in the pan whole (no need to peel them).
12. Use a teaspoon to scoop up the garlic infused oil and baste the Salmon steaks.
13. Once the Salmon is no longer stuck to the pan and has cooked about halfway up the sides, use a pair of tongs to sear it on each side, allowing it to cook until the steak comes free from the pan without sticking and has a brown colour.
14. Serve Salmon steak on a bed of the Taboulleh Salad, drizzled with a little Lemon juice from the remaining half of the lemon.
