# Pasta al Pomodoro

A rich thick tomato sauce with fresh basil (serves 2).

## Ingredients
- 500g of fresh tomatoes (sliced)
- 1 white onion (diced)
- 2 sprigs of basil (finely diced) + 4 leaves to garnish
- 2 tsp dried oregano
- 2 cloves of garlic (finely diced or crushed)
- 50g of parmesan cheese (grated)
- 1 Vegetable stock cube
- Olive oil
- 150g pasta

## Steps
1. On a low heat gently simmer the onion until translucent (about 10min). Then add the garlic and tomatoes.
2. Mix the stock cube with 100ml of boiling water, then add it to the pan.
3. Add the basil, oregano, parmesan, and some salt. Mix all the ingredients together thoroughly. Leave this to simmer for at least 30min more, checking regularly. The sauce is thick enough when it sticks to the back of a spoon.
4. In a separate pan cook some pasta in very salty water for slightly less than the packet suggests.
5. Once the pasta is done, splash a tablespoon of pasta water into the tomato sauce, drain the pasta.
6. Toss the pasta in a little olive oil, before mixing it into the tomato sauce. Cook for an additional 30sec before turning off the heat.
7. Serve immediately with two leaves of basil on top to garnish.