# Bean Chilli Sauce

Delicious served with rice or as part of a tasty Mexican dish (see recipes 7,8 and 9). Serves 4.

## Ingredients
- 1/2 a courgette (diced)
- 1 carrot (diced)
- 1/2 a yellow/red bell pepper (diced)
- A handful of mushrooms (diced)
- 1 can of kidney beans
- 200g tomatoes (diced)
- 1 onion (finely diced)
- A handful of fresh coriander (chopped)
- 1 vegetable stock cube
- Oil
- Three cloves of garlic
- 1 and 1/2 tsp chili powder
- 1 and 1/2 tsp Smoked paprika
- 1/2 tsp Cumin seeds
- 1 tsp Cumin powder
- 1 tbsp Tomato puree

## Method
1. Fry the onion for 1-2 minutes on a medium to low heat.
2. Throw in the cumin seeds and garlic, fry for 1 more minute.
3. Add the carrot, pepper, mushrooms, courgette, smoked paprika and cumin powder.
3. Meanwhile, add the stock cube to 200ml of boiling water, whisk with a fork until dissolved.
4. After 5 minutes, stir in the tomatoes, coriander, chili powder, stock, kidney beans and tomato puree.
5. Let simmer for at least 30 minutes, stirring occasionally and adding extra water if the sauce starts sticking to the bottom of the pan.
