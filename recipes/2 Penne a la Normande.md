# Pâtes à la Normande

A creamy mushroom and parsley pasta sauce, serves 2.

## Ingredients
- 300g Chestnut Mushrooms (thinly sliced)
- 120g Shiitake Mushrooms (stems removed, torn in half)
- 100ml White wine 
- 150ml Crème Fraîche
- Half a white onion (diced)
- 2 cloves of Garlic
- Knob of butter (not margarine)
- Handful of Fresh Parsley (finely chopped)
- 150g Gigli Pasta
- Olive Oil

## Steps
1. Heat the pan on medium heat and add the mushrooms (without oil). Cook until the water inside the mushrooms leaks out and mostly evaporates.
2. Add the onion, garlic, and butter, cook until the onion turns slightly translucent (about 8min).
3. In a separate pan add boiling water, a lot of salt and the pasta. Cook for 8min or until almost done.
4. While the pasta is cooking add the Parsley, white wine and Crème Fraîche, stir thoroughly until a creamy sauce is formed (If the sauce is too thick add more wine and if it is too thin add more Crème Fraîche). Set too a low heat to avoid boiling off all the sauce, but enough to boil off the alcohol.
5. When the pasta is done pour a small amount of the water into the main pan, and drain the rest.
6. Toss the pasta in a small amount of Olive oil, then add it to the rest of the pan. Mix thoroughly so that all pasta is coated in the sauce.
7. Garnish with black pepper, and serve.
