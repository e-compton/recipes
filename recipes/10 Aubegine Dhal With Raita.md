# Aubergine Dhal with Raita

A comforting chickpea and lentil curry, served with a creamy spiced yoghurt. Serves 4.

## Ingredients
- 1 large onion (thinly sliced)
- 3 garlic cloves (crushed)
- 1 tbsp ginger (finely chopped)
- 2 tsp turmeric
- 2 tsp ground coriander
- 2 tsp cumin seeds
- 1 red chilli, seeded and thinly sliced
- 1 vegetable stock cube
- 115g red lentils
- 1 large aubergine (cubed)
- 1 can chickpeas (no need to drain)
- 2 cups of cooked rice

### Raita
- 2 tsp cumin seeds
- 2 tsp mustard seeds
- 200ml yoghurt
- Half a cucumber (skinned and diced with seeds removed)

## Method
1. Heat the oil in a large non-stick pan and fry the onion, garlic and ginger for 8 mins until softened. 2. Tip in the turmeric, coriander, cumin and chilli and cook for 20 seconds, stirring occasionally.
3. Dissolve the stock cube in 800ml boiling water.
3. Stir in the stock, lentils and aubergine, and cover the pan and leave to simmer for 15 mins until the lentils are pulpy. 
4. In a small sauce pan, start the Raita. Add the cumin and mustards seeds with a little oil. Cook on low heat for about 2minutes or until fragrant.
5. In a bowl, thoroughly mix together the seeds with the yoghurt and cucumber. Salt to taste.
6. Stir in the chickpeas with their water, if you need a little more liquid, and cook for 5 mins.
7. Serve the Dhal on a bed of rice, with a dollop of Raita on top.
