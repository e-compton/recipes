# Burritos

Flour tortillas packed with a spicy chilli and other fillings. Serves 4.

## Ingredients
- Chilli (see recipe 4)
- Guacamole (recipe pg...)
- 4 portions of Scientific Rice
- 50g Grated cheddar cheese
- 4 wraps
- 300g southern fried chicken fillets

## Method
1. Cook the chicken.
2. Warm each tortilla up. (can microwave for 10 secs each)
3. Line the fillings up in a line the center of each tortilla, be careful not to overfill and to leave a gap at the edge.
4. To wrap:
 - Fold the two ends of the wrap, where the line of filling ends, over the filling.
 - While holding those two ends down, bring one of the other edges over the filling and scoop the filling under it, wrapping it tightly.
 - Roll the tortilla together into a secure wrap.
