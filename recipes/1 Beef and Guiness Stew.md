# Beef and Guinness Stew

A thick beefy stew served with toasted cornbread. Serves 4.

## Ingredients
- 500g Diced Beef (preferably brisket)
- 500ml Guinness
- Two large white onions (diced)
- Two sticks of celery (sliced thinly)
- Two large carrots (sliced thinly)
- Two large portobello mushrooms (torn into pieces)
- Two sprigs of fresh rosemary
- Two cloves of garlic (chopped finely or pressed)
- Knob of butter (not margarine)
- 1 Beef stock cube
- 1 Loaf of corn bread

## Steps
1. In a large heavy bottomed pan, add the butter, mushrooms, and onions. Cook for 5min or until slightly translucent.
2. Switch to a high heat. Add the garlic, carrots, celery, and beef. Stir continuously until the meat has browned on the outside (5min).
3. Season with salt and add the sprigs of rosemary.
4. In a mug mix together boiling water and the stock cube. Once fully dissolved, add the stock to pan along with all of the Guinness.
5. Bring back to a boil and cover. Leave simmering on a low heat for 2 hours, stirring every 30min to prevent it sticking. If the ingredients look dry add more boiling water.
6. The stew is sufficiently thick when the sauce coats the back of a spoon.
7. Pick out the sprigs of rosemary and serve in a bowl with slices of toasted corn bread for dipping.
