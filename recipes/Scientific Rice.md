# Scientific Rice

When you cook rice, most of the water is absorbed by the rice, but some evaporates. To get perfect rice every time the evaporated water must be accounted for. The radius of your pan will determine how much water evaporates. The wider the pan, the more water will evaporate. Try adding 1/3 of a cup to begin with, then experiment to get the perfect rice.

- 1 cup of water for 1 cup of rice
- Add 0.25 - 0.5 cups depending on pan's radius

Steps
1. Rinse the rice in a sieve until all the starch has been washed off (this can take a while).
2. Add the rice and cold water to a pan with a lid and put on a medium heat.
3. Cook for 20min or until all the water is gone (do not remove the lid).
4. Once cooked, remove the lid and leave for 10minutes for the remaining water to evaporate.

Tips
- The perfect rice is not sticky, the grains hold the structure, but are soft.
- Rice cooked with too much water will look mushy and the grains will fall apart when stirred.
- Rice cooked with too little water will either burn or be undercooked and crunchy when all the water is gone.