# Cheese Burgers

## Ingredients

- 250g Beef Mince
- 2 Brioche Buns
- Half a Beef Tomato (sliced horizontally)
- Half a Red Onion (slice horizontally)
- 2 slices smoked Applewood cheese
- Ketchup
- Mayonnaise
- Knob of Butter
- Salt

## Steps
1. In a frying pan place the brioche buns (sliced in half) face down on a low heat to toast.
2. Put a second frying pan on high heat and some oil.
3. Mold half the beef into a ball then flatten in your hands into a very flat circle. Salt one side and place it face down on the pan, then salt the other side. Repeat with the other half of the mince meat.
4. Add the knob of butter to the Brioche pan, and make sure all four pieces have some melted butter under them. 
5. When the burgers no longer stick to the pan, and look brown/crispy on the bottom, flip them over. Place a slice of the cheese on top of each.
6. Once the brioche buns are brown and dry on the bottom, remove them from the heat and place them on the plates.
7. When the burgers no longer stick to the pan again, remove from heat and leave to rest for at least 5min.
8. On the bottom half of each brioche bun add ketchup and mayonnaise. Then add the burger patty, followed by the ring of tomato and the ring of red onion.