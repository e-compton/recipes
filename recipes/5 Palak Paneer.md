# Palak Paneer

Indian cheese submerged in a creamy spinach sauce, usually served with rice.
Serves 2-3.

## Ingredients
- 250g fresh spinach (chopped)
- 225g Paneer
- 2 tbsp cream / soy cream (optional)
- 1 large tomato (roughly chopped)
- 1 red onion (roughly chopped)
- 1 handful fresh coriander
- a pinch of nutmeg
- 2 bay leaves
- 1/2 tsp cumin seeds
- 1/2 tsp coriander powder
- 1/2 tsp cumin powder
- 1/2 tsp chili powder
- small piece of ginger (grated)
- 5-6 cloves garlic
- 2-3 tbsp plain yoghurt
- pinch of turmeric
- pinch of sugar
- oil

## Steps
1. Fry cumin seeds and 1 bay leaf in oil on a medium heat.
2. Add ginger and garlic.
3. Add onion and a pinch of salt.
4. Once the onion is soft and slightly brown, add the tomato.
5. After a few minutes, mix in the yoghurt, turmeric, coriander powder, cumin powder, red chili powder, sugar and nutmeg.
6. Throw in the spinach and cook until wilted.
7. Turn off the heat, add the fresh coriander.
8. Blend the sauce until you can only see fine specks of spinach in the sauce.
9. Return the sauce to the pan over a medium heat, add in a generous amount of salt and some cream if desired.
10. Grate a handful of paneer into the sauce, chop the rest into squares.
11. Add the chopped paneer and it is ready to serve!