# Smoothie

A thick, healthy, bright green smoothie. Serves 1.

## Ingredients
- 100g kale
- 3 ice cubes
- 200ml water
- 1/2 banana (sliced)
- 10g flax seeds
- 1/2 apple or 1/2 mango (chopped)

## Steps
1. Blend the ingredients together, adding water if necessary.
