# Quesadillas

Cheesy, melt in the mouth tortillas with a spicy filling. Serves 4.

## Ingredients
- 4 Tortillas
- 150g Cheddar cheese
- 1 red/yellow bell pepper (diced)
- 1 or two tomatoes (diced)
- 1/2 courgette (diced)
- 1/2 carrot (diced)
- a handful of mushrooms (diced)
- 1 onion (diced)
- 2 garlic cloves (crushed)
- 1/2 tsp cumin seeds
- 1/2 tsp cumin powder
- 1 tsp smoked paprika
- 1/2 tsp chili powder
- 1 handful fresh coriander
- Oil

## Method
1. Fry the onion, garlic and cumin seeds for 1-2 minutes on a medium to low heat.
2. Throw in the carrot, pepper, mushrooms, courgette, cumin powder and smoked paprika.
3. Add tomatoes, chili powder and enough water to simmer until the vegetables are soft.
4. When the sauce is ready, heat a tortilla in some oil on a medium heat in a pan.
5. Add a layer of cheese, filling, cheese, all on one half of the tortilla.
6. Fold in half, then brown on each side.
7. The tortilla is cooked when crunchy and the cheese inside is fully melted.
