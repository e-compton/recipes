# Spanish Lentil Stew

Hearty and comforting, this stew is great for warming up on a chilly evening. Serves 4.

## Ingredients
- 1 onion (finely diced)
- 2 cloves of garlic (crushed)
- 1/2 a bell pepper (finely diced)
- 1 large tomato (finely diced)
- 1 1/2 teaspoons smoked paprika
- 1 bay leaf
- 2 medium carrots (peeled and finely diced)
- 1 large potato (peeled and finely diced)
- 250g dried lentils (rinsed)
- 2 tablespoons soy sauce

## Method
1. Heat a medium-sized pot over medium heat and add a splash of water, the onion, garlic, pepper and tomato, until all are very soft, adding water if necessary.
2. Add the paprika and bay leaf.
3. Allow to cook for about 2 minutes to bring out the flavour while stirring so it doesn’t burn.
4. Add the water, carrots, potato and lentils. 
5. Bring to a boil then cover and reduce the heat to low. Simmer for about 30 minutes, or until the lentils are soft and creamy.
6. When the lentils are ready, add the soy sauce, salt and pepper to taste.
7. This stew can be served thick or thin. Add more water if you want it soupier, but note that it will thicken as it cools.