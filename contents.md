# Contents

## Dinners
1. Beef and Guinness Stew DONE
2. Penne a la Normande DONE
3. Salmon and Taboulleh DONE
4. Egg Curry ED AND CHAR
5. Palak Paneer DONE
6. Bean Chilli DONE
7. Burritos DONE
8. Enchiladas DONE
9. Quesadillas DONE
10. Aubergine Dhal with Raita DONE
11. Pasta al Pomodoro DONE
12. CheeseBurgers DONE
13. Cauliflower Soup ED
14. Focaccia DONE
15. Egg Salad ED
16. Greek Salad DONE
17. Onion soup CHAR
18. Ramen DONE
19. Lentil Stew DONE
20. Potato + Tomato curry CHAR

## Sides
1. Guacamole DONE
2. Salsa DONE
3. Refried Beans DONE
4. Chapatis CHAR
5. Green Bean and Potato Rice CHAR
6. Egg fried rice DONE
7. Scientific rice DONE

## Breakfast
1. Granola DONE
2. Smoothie DONE